/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */
exports.justhello = (req, res) => {
    let message = req.query.message || req.body.message || 'Just Hello v4 !!';
    res.status(200).send(message);
};
