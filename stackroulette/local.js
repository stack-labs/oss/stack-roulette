const express = require("express");
const { stackroulette } = require('./stackroulette');

const server = express();

server.listen(3000);
server.use(express.json());

server.post("/stackroulette", stackroulette);
