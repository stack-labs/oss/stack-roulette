const { google } = require("googleapis");

function generateGroup(mails) {
  let users = mails.sort(() => 0.5 - Math.random());
  let groups = [];

  while (users.length > 4) {
    const [user1, user2, user3, user4, ...others] = users
    groups = [...groups, [user1, user2, user3, user4]]
    users = others
  }

  for (let i = 0; i < users.length; i++) {
    groups[i].push(users[i])
  }

  return groups;
}

async function sleep(s) {
  return await new Promise(resolve => setTimeout(resolve, s * 1000));
}

async function insertEvents(client, group, numberOfRetry = 10) {
  try {
    return await _insertEvents(client, group)
  } catch(e) {
    console.log("error", e);
    if (numberOfRetry === 0) {
      console.log("error, no more retry for ", group);
      return;
    }
    if (e.code === 403) {
      await sleep(10)
    }

    return insertEvents(client, group, numberOfRetry-1)
  }
}

function nextFriday() {
  let date = new Date();
  date.setDate(
      date.getDate()
      + 7 /* next week, not current one */
      + (5 /* is friday */ + 7 - date.getDay()) % 7
  );
  return date;
}


async function _insertEvents(auth, group) {
  let startDate = nextFriday();
  let endDate = nextFriday();

  startDate.setHours(11, 45, 0, 0);
  endDate.setHours(12, 0, 0, 0);

  const attendees = group
      .map(it => ({ email: it }));

  const event = {
    summary: "Stack Roulette : It's Coffee Time ☕😀",
    location: "Partout, mais avec un café",
    description:
      "##########################\nBot Cafe Roulette\n##########################\n\n Vous avez été tiré au sort dans un groupe de 4 personnes au sein de l'organisation, vous êtes invité à rejoindre le groupe Google Meet créé afin de discuter et partagez entre vous. \n\nPréparez votre café et découvrez vos nouveaux partenaires.\n\n\n ☕☕☕",
    start: {
      dateTime: startDate.toISOString(),
      timeZone: "Europe/London",
    },
    end: {
      dateTime: endDate.toISOString(),
      timeZone: "Europe/London",
    },
    attendees: attendees,
    reminders: {
      useDefault: false,
      overrides: [
        { method: "email", minutes: 24 * 60 },
        { method: "popup", minutes: 10 },
      ],
    },
    conferenceData: {
      createRequest: {
        requestId: Math.random().toString(36).substring(2, 10),
        conferenceSolution: {
          key: {
            type: "hangoutsMeet",
          },
        },
      },
    }
  };

  return insertEventsCalendar(auth, event);
  // return Promise.resolve(event)
  // return Promise.reject(event)
}

async function insertEventsCalendar(auth, event) {
  const calendar = google.calendar({ version: "v3", auth });
  return new Promise((resolve, reject) => {
    calendar.events.insert(
      {
        auth: auth,
        calendarId: "c_pm7degqjj5s8f0qcvhrlngt9u8@group.calendar.google.com",
        resource: event,
        sendUpdates: "all",
        conferenceDataVersion: 1,
      },
      function (err, event) {
        if (err) {
          console.log(
            "There was an error contacting the Calendar service: " + err.message
          );
          reject(err);
        } else {
          console.log("Event created: %s", event.data.htmlLink);
          resolve(event);
        }
      }
    );
  });
}

async function listUsers(auth) {
  await auth.authorize();
  const service = google.admin({ version: "directory_v1", auth });
  const res = await service.users.list({
    customer: "my_customer",
    maxResults: 100,
    orderBy: "email",
  });

  return res.data.users
      .filter(it => it.orgUnitPath === "/Canada" || it.orgUnitPath === "/France")
      .filter(it => it.suspended !== true)
      .map(it => it.primaryEmail);
}

module.exports = {
  insertEvents,
  listUsers,
  generateGroup
};
