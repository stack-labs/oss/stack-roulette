const { google } = require("googleapis");
const {
  generateGroup,
  listUsers,
  insertEvents,
} = require("./modules/events");

// https://stackoverflow.com/questions/43456630/access-google-drive-api-from-a-google-cloud-function
let privatekey = require("./creds/key.json");

const scopes = [
  "https://www.googleapis.com/auth/calendar",
  "https://www.googleapis.com/auth/admin.directory.user.readonly",
];

let client = new google.auth.JWT(
    privatekey.client_email,
    null,
    privatekey.private_key,
    scopes,
    "kevin.davin@stack-labs.com"
);

client.authorize(function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log("Successfully connected!");
  }
});


const stackroulette = async (req, res) => {
  const isProd = (req.body || {}).prod === true
  try {
    const users = isProd
        ? (await listUsers(client))
        : ["k+1@stack-labs.com", "k+2@stack-labs.com", "k+3@stack-labs.com", "k+4@stack-labs.com", "k+5@stack-labs.com"];

    const groups = generateGroup(users)

    let events = []
    for (const group of groups) {
      events = [...events, await insertEvents(client, group)];
    }
    res.status(200).send({message: "events created", groups, events});
  } catch (err) {
    console.log(err);
    res.status(500).send({ err });
  }
}

exports.stackroulette = stackroulette
